/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance2;

/**
 *
 * @author sairu
 */
public class testshape {

    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        circle1.print();
        Circle circle2 = new Circle(4);
        circle2.print();
        Triangle triangle = new Triangle(4, 3);
        triangle.print();
        Rectangle rectangle = new Rectangle(3, 4);
        rectangle.print();
        Rectangle square = new Rectangle(2, 2);
        square.print();

        shape[] shape = {circle1, circle2, triangle, rectangle, square};
        for (int i = 0; i < shape.length; i++) {
            System.out.println();
            shape[i].printt();
            shape[i].print();

            System.out.println();
        }
    }
}
