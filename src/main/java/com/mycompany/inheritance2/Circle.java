/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance2;

/**
 *
 * @author sairu
 */
public class Circle extends shape {
    private double r;
    public static final double pi = 22.0/7;
    
    public Circle(double r){
        System.out.println("Circle created");
        this.r = r;
    }
    
    @Override
    public double calArea(){
        return pi*r*r;
    }
    
    public void print(){
        System.out.println("CalArea: "+calArea());
    }
    public void printt() {
        System.out.println("Shape created: Circle");
    }

}
