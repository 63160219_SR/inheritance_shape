/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance2;

/**
 *
 * @author sairu
 */
public class Rectangle extends shape {

    private double h;
    private double w;
    private double side;

    public Rectangle(double h, double w) {
        if (h != w) {
            System.out.println("Rectangle created");
            this.h = h;
            this.w = w;
        } else {
            System.out.println("Square created");
            this.h = h;
            this.w = w;
        }
    }

    @Override
    public double calArea() {
        return h * w;
    }

    public void print() {

        System.out.println("CalArea: " + calArea());
    }

    public void printt() {
        if(h!=w){
            System.out.println("Shape created: Rectangle");
        }
        else{
            System.out.println("Shape created: Square");
        }
    }
}
