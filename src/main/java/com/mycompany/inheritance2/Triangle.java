/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance2;

import static com.mycompany.inheritance2.Circle.pi;

/**
 *
 * @author sairu
 */
public class Triangle extends shape{
    private double base;
    private double height;
    
    public Triangle(double base,double height){
        System.out.println("Triangle created");
        this.base = base;
        this.height = height;
    }
    
    @Override
    public double calArea(){
        return 0.5*base*height;
    }
    
    public void print(){
        System.out.println("CalArea: "+calArea());
    }
    public void printt() {
        System.out.println("Shape created: Triangle");
    }
}
